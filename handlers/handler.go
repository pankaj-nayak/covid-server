package handlers

import (
	addservice "addservice/proto"
	"context"
	"fmt"
	"sync"

	"github.com/kelvins/geocoder"
	"github.com/twmb/algoimpl/go/graph"
	"github.com/umahmood/haversine"
)

// Server ... This is our GRPC server
type Server struct {
	addservice.UnimplementedAddServiceServer
	//	savedFeatures []addservice.Human
	//mu            sync.Mutex
}

var nodelist = make(map[float64]graph.Node, 0)
var g = graph.New(graph.Undirected)
var l = sync.Mutex{}

// Add ... Function used for learning Grpc server and its client communication.
func (*Server) Add(ctx context.Context, in *addservice.AddRequest) (*addservice.AddResponse, error) {

	fmt.Println("this jaknap is working : ", in.A, in.B, in.C)
	//fmt.Printf(in.A + in + c)

	// check if node already exist
	//node, found := nodelist[in.C]

	// Create a new node :
	l.Lock()
	value, ok := nodelist[in.C]
	fmt.Println(value)
	if false == ok {
		nodelist[in.C] = g.MakeNode()
	}
	var node graph.Node = nodelist[in.C]
	node.Lat = in.A
	node.Lon = in.B
	node.Id = in.C
	//node.Value = in.C
	nodelist[in.C] = node
	*nodelist[in.C].Value = in.C
	//node, found := nodelist[in.C]

	for key, element := range nodelist {

		//fmt.Println(nodelist[key])
		var currentnode = haversine.Coord{Lat: in.A, Lon: in.B}
		var graphnode = haversine.Coord{Lat: element.Lat, Lon: element.Lon}
		fmt.Println("value of graph node Lat and Log : \n", key, element.Lat, element.Lon)
		mi, km := haversine.Distance(currentnode, graphnode)
		fmt.Println("value of distance in mi,  km: \n", mi, km*1000)

		var metre = km * 1000
		if metre < 10 && metre != 0 {
			g.MakeEdgeWeight(nodelist[in.C], element, int(metre))
			fmt.Printf(" Edges : (%v, %v) : Distance %v\n", nodelist[in.C].Id, key, metre)
		}
	}

	var i int = 0
	for key, element := range nodelist {
		var neighbour []graph.Node = g.Neighbors(element)
		fmt.Printf("{ [%v] : key : %v", i, key)
		i++
		if len(neighbour) == 0 {
			fmt.Printf("}\n")
		}
		if len(neighbour) != 0 {
			fmt.Printf(": NEIGHBOURS ")
			for i := range neighbour {
				fmt.Printf(": %v ", (*neighbour[i].Value))
			}
			fmt.Printf("}\n")
		}
	}
	l.Unlock()
	return &addservice.AddResponse{Result: in.A + in.B + in.C}, nil
}

// RegisterHuman ... First call when user will instasll the app
// Later we will be picking up this value from the device shared preferences
func (c *Server) RegisterHuman(ctx context.Context, in *addservice.Human) (*addservice.Identity, error) {
	fmt.Println("entered RegisterHuman : ", in.GetDeviceid())

	fmt.Println("Exiting RegisterHuman")
	return &addservice.Identity{}, nil
}

//UpdateLocation ... Device will be continously updating its coordinates with the server
func (c *Server) UpdateLocation(ctx context.Context, in *addservice.GeoLocationRequest) (*addservice.GeoLocationResponse, error) {

	fmt.Println("WHY BELOW LOGS NOT COMING!!Entered UpdateLocation")
	fmt.Printf("dfdfdUpdateLocation : Lat : %v , Lon : %v , Name : %v \n", in.GetLocation().GetLatitude(), in.GetLocation().Longitude, in.Location.GetName())
	fmt.Println("kuch to ho benchooo!!")

	// See all Address fields in the documentation
	address := geocoder.Address{
		Street:  "Central Park West",
		Number:  115,
		City:    "New York",
		State:   "New York",
		Country: "United States",
	}

	// Convert address to location (latitude, longitude)
	location, err := geocoder.Geocoding(address)

	location = geocoder.Location{
		Latitude:  in.Location.Latitude,
		Longitude: in.Location.Longitude,
	}
	// Convert location (latitude, longitude) to a slice of addresses
	addresses, err := geocoder.GeocodingReverse(location)

	if err != nil {
		fmt.Println("Could not get the addresses: ", err)
	} else {
		// Usually, the first address returned from the API
		// is more detailed, so let's work with it
		address = addresses[0]

		// Print the address formatted by the geocoder package
		fmt.Println(address.FormatAddress())
		// Print the formatted address from the API
		fmt.Println(address.FormattedAddress)
		// Print the type of the address
		fmt.Println(address.Types)
	}
	fmt.Println("Exiting UpdateLocation")
	return &addservice.GeoLocationResponse{}, nil
}
