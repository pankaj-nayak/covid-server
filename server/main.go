package main

import (
	"addservice/handlers"
	addservice "addservice/proto"
	"fmt"
	"net"

	"google.golang.org/grpc"
)

/*
func newServer() *addservice.AddServiceServer {
	s := &routeGuideServer{routeNotes: make(map[string][]*pb.RouteNote)}
	s.loadFeatures(*jsonDBFile)
	return s
}
*/
func main() {

	lis, err := net.Listen("tcp", ":8033")
	if err != nil {
		fmt.Println(err)
		lis.Close()
	}
	defer lis.Close()

	addServ := handlers.Server{}

	grpcServer := grpc.NewServer()

	fmt.Println("Listening on port number : 8033 ...jaknap21 ")
	addservice.RegisterAddServiceServer(grpcServer, &addServ)
	if err := grpcServer.Serve(lis); err != nil {
		fmt.Println(err)
	}
	defer grpcServer.Stop()
}
